/*
 The objective of this file is to limit the maximum number of times one text pattern of warnings is present in a jest output file.
 This script is intended to be called like this:
  node validate-warnings.js <path-to-jest-output-file>
*/
const fs = require("fs")
const readline = require("readline")
const {
  S3Client,
  PutObjectCommand,
  GetObjectCommand,
} = require("@aws-sdk/client-s3");

// S3 config
const REGION = "eu-west-3"
const bucketName = "test-warnings";
const keyName = "jest-warning-restrictions.json";
const s3 = new S3Client({ region: REGION });

async function run() {
  try {
    console.log("Starting to check warnings restrictions")
    const jestResultsPath = process.argv[2]
    if (!jestResultsPath) {
      console.error("You must provide a file with the output of jest")
      process.exit(1)
    }
    const warningLines = await parseWarningLines(jestResultsPath)
    const warningsRestrictions = await getLatestWarningsRestrictions()
    const [failedRestrictionsIndexes, currentRestrictionsCount] = processWarnings(warningsRestrictions, warningLines)
    if (failedRestrictionsIndexes.size) {
      printViolations(warningsRestrictions, failedRestrictionsIndexes, currentRestrictionsCount)
      process.exit(1)
    } else {
      await updateWarningRestrictions(warningsRestrictions, currentRestrictionsCount)
    }
  } catch (err) {
    console.log("There was an error while processing warning restrictions", err)
  }
}

async function getLatestWarningsRestrictions() {
  const response = await s3.send(new GetObjectCommand({ Bucket: bucketName, Key: keyName }))
  const chunks = []

  for await (const chunk of response.Body) {
    chunks.push(chunk)
  }

  const buffer = Buffer.concat(chunks)
  return JSON.parse(buffer.toString("utf-8"))
}

/**
 * Reads contents of output file and returns only lines after console.warn or console.error
 */
async function parseWarningLines(outputFilePath) {
  const lineReader = readline.createInterface({
    input: fs.createReadStream(outputFilePath),
    output: process.stdout,
    terminal: false
  })
  const warnings = []
  let lastLineIsWarningHeader = false
  for await (const line of lineReader) {
    const isWarningHeader = /\s+console\.(warn|error)$/.test(line)
    if (isWarningHeader) {
      lastLineIsWarningHeader = true
      continue
    }
    if (lastLineIsWarningHeader) {
      warnings.push(line)
      lastLineIsWarningHeader = false
    }
  }
  return warnings
}

/**
 * Finds restriction violations and current number of instances of a certain restriction
 */
function processWarnings(restrictions, warningLines) {
  const currentRestrictionsCount = Array(restrictions.length).fill(0)
  const failedRestrictionsIndexes = new Set()
  for (const warningLine of warningLines) {
    for (const [restrictionIndex, restriction] of restrictions.entries()) {
      if (matchesRestriction(warningLine, restriction)) {
        currentRestrictionsCount[restrictionIndex] += 1
        if (currentRestrictionsCount[restrictionIndex] > restriction.max) {
          failedRestrictionsIndexes.add(restrictionIndex)
        }
      }
    }
  }
  return [failedRestrictionsIndexes, currentRestrictionsCount]
}

function matchesRestriction(warning, restriction) {
  for (const pattern of restriction.patterns) {
    if (warning.includes(pattern)) {
      return true
    }
  }
  return false
}

function printViolations(restrictions, failedRestrictionsIndexes, currentRestrictionsCount) {
  console.error("The following warning violations were found:");
  for (const index of failedRestrictionsIndexes) {
    const expectedCount = restrictions[index].max
    const currentCount = currentRestrictionsCount[index]
    const patterns = restrictions[index].patterns.join(",")
    console.error(`- For paterns "${patterns}", the expected is ${expectedCount}, actual is ${currentCount}`)
  }
}

async function updateWarningRestrictions(restrictions, currentRestrictionsCount) {
  const updatedWarningsRestrictions = restrictions.map((warningRestriction, index) => ({
    ...warningRestriction,
    max: currentRestrictionsCount[index]
  }))
  const objectParams = {
    Bucket: bucketName,
    Key: keyName,
    Body: JSON.stringify(updatedWarningsRestrictions)
  };
  await s3.send(new PutObjectCommand(objectParams));
}

run();