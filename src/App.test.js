import { render, screen, wait } from '@testing-library/react';
const numbers = ["1", "1"]

const App = () => (
  <div>
    {numbers.map(item => <li>Hello</li>)}
    <div>Hello world</div>
  </div>
)

test('renders learn react link', async () => {
  render(<App />);
  await wait(() => expect(screen.getByText("Hello world")).toBeInTheDocument())
});
