#!/bin/bash

# Install dependencies
yarn

# Run tests and store results into a text file
yarn test src -- --forceExit --colors --ci 2>jest-output.txt

# Validate warnings
yarn validate-warnings jest-output.txt
